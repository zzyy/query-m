/**
 * Created by zzy on 2018/11/7.
 */
const apiModule = {
  // 请求转发
  forwardReq: {
    url: '/api/requestForward1',
    method: 'post'
  },
  // // 登录
  // getAuth: {
  //   url: '/api/auth',
  //   method: 'post'
  // },
  // 查询民族 籍贯等
  getListForBase: {
    url: '/api/baseinfo/getListForBaseJson',
    method: 'get'
  },
  // 查询地点数据
  getListForPlace: {
    url: '/api/baseinfo/getListForPlaceForCode',
    method: 'get'
  },
  // 图片上传地址
  upLoadImgUrl: {
    url: '/api/colligation/uploadImg',
    method: 'post'
  },
  // 上传图片下载
  downloadImg: {
    url: '/api/colligation/downloadImg',
    method: 'get'
  },
  // 人员信息
  getPersForPage: {
    url: '/api/colligation/getPersForPage',
    method: 'post'
  },
  // 布控信息
  getMoniForPage: {
    url: '/api/colligation/getMoniForPage',
    method: 'post'
  },
  // 通行信息
  getCrossForPage: {
    url: '/api/colligation/getCrossForPage',
    method: 'post'
  },
  // 比对信息
  getListForRsult: {
    url: '/api/colligation/getListForRsult',
    method: 'post'
  },
  // 比对信息详情
  getResultForDetail: {
    url: '/api/colligation/getResultForDetail',
    method: 'post'
  },
  // 人员基本信息详情
  getPersonInfo: {
    url: '/api/colligation/getPersonInfo',
    method: 'post'
  },
  // 人像查询
  getResultByHttp: {
    url: '/api/colligation/getResultByHttp',
    method: 'post'
  },
  // 人像比对历史查询列表接口
  getHistoryList: {
    url: '/api/colligation/getHistoryList',
    method: 'post'
  },
  // 人像比对历史记录详细列表
  getMoniListById: {
    url: '/api/colligation/getMoniListById',
    method: 'post'
  },
  // 布控人员轨迹
  getCrossByMoni: {
    url: '/api/colligation/getCrossByMoni',
    method: 'post'
  }
}

// const apiSetting = {...apiModule}

export default apiModule
