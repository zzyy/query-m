import axios from 'axios'
// import router from '../../router'
import api from '../../api'
this.$api = api

const root = process.env.API_URL
// 部署到线上打开
let token = localStorage.getItem('token')
// let token = process.env.TOKEN

// 请求时
axios.interceptors.request.use(
  config => {
    // this.bus.$emit('loading', true)
    if (token !== null) {
      config.headers.Authorization = token
    } else {
      // router.push({name: 'login'})
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 请求完成
axios.interceptors.response.use(
  response => {
    // this.bus.$emit('loading', false)
    return response
  },
  error => {
    return Promise.resolve(error.response)
  }
)

function successState (res) {
  // 判断后台返回错误码
  if (res.data.code === 500 || res.data.code === 600 || res.data.code === 700) {
    alert(res.data.message)
  }
}

function errorState (err) {
  // 状态码为200，直接返回数据
  if (err && (err.status === 200 || err.status === 304 || err.status === 400)) {
    return err
  } else {
    console.log('网络异常')
  }
}

const httpServer = (opts, data) => {
  // 转发参数
  let forward = {
    method: this.$api.forwardReq.method,
    url: this.$api.forwardReq.url
  }

  // 转发接口请求类型
  let type
  if (opts.method === 'get') {
    type = false
  } else if (opts.method === 'post') {
    type = true
  }

  // 转发
  let transmit = {
    methodUrl: opts.url,
    content: data || {},
    token: token,
    requestType: type
  }

  let Public = {}
  let httpDefaultOpts = {
    method: forward.method,
    url: forward.url,
    timeout: 1000 * 60,
    baseURL: root,
    // get参数
    params: Object.assign(Public, transmit),
    data: Object.assign(Public, transmit),
    headers: forward.method === 'get' ? {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    } : {
      'X-Requested-with': 'XMLHttpRequest',
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (forward.method === 'get') {
    delete httpDefaultOpts.data
  } else {
    delete httpDefaultOpts.params
  }

  return new Promise((resolve, reject) => {
    axios(httpDefaultOpts)
      .then(res => {
        successState(res)
        resolve(res.data)
      })
      .catch(err => {
        errorState(err)
        reject(err)
      })
  })
}

export default httpServer
