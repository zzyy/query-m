/**
 * Created by zzy on 2018/10/24.
 */
import { Tabbar, TabbarItem } from 'vux'
export default {
  name: 'tab-bar',
  components: {
    Tabbar,
    TabbarItem
  }
}
