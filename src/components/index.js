/**
 * Created by zzy on 2018/10/24.
 */
import TabBar from './tabbar'
import Scroll from './scroll'
import Loading from './loading'
export {
  TabBar,
  Scroll,
  Loading
}
