import SurveyInfo from './SurveyInfo.vue'
import SurveyPerson from './survey-person'
export {
  SurveyPerson,
  SurveyInfo
}
