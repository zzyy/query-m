import { Scroll, Loading } from 'components'
import { getStore } from '../../../../utils/storage'
export default {
  name: 'survey-person',
  data () {
    return {
      loading: true,
      pullup: true,
      page: 0,
      personData: {},
      crossData: [],
      // 查询条件
      searchData: {},
      peopleParams: { startTime: '', endTime: '', personName: '', personSex: '', personId: '', personRace: '', personRegioncode: '', personCardnum: '', page: 1, size: 10 },
      crossParams: { moniId: '', page: 1, size: 10 }
    }
  },
  mounted () {
    this.$nextTick(function () {
      this.$refs.wrapper._initScroll()
      this.getSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    getSearchData () {
      this.searchData = JSON.parse(getStore('searchData'))
      this.peopleParams.personId = this.searchData.personId
      this.crossParams.moniId = this.searchData.id
      this.queryPersonInfo()
    },
    // 查询人员信息
    queryPersonInfo () {
      this.$ajax(this.$api.getPersForPage, this.peopleParams).then(
        res => {
          if (res.code === 200) {
            if (res.content.count) {
              this.personData = res.content.list[0]
              this.personData.personPhoto = this.imgJoint(this.personData.personPhotoUrl)
            }

            this.queryPersonCross()
          }
        }
      )
    },
    // 查询人员轨迹
    queryPersonCross () {
      this.$ajax(this.$api.getCrossByMoni, this.crossParams).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.crossData = this.crossData.concat(res.content.list)
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        }
      )
    },
    // 加载更多方法
    loadMore (p) {
      this.crossParams.page = p
      this.loading = true
      this.queryPersonCross()
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
