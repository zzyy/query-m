import { Scroll, Loading } from 'components'
import { setStore, getStore } from '../../../utils/storage'
export default {
  name: 'survey-info',
  data () {
    return {
      loading: true,
      pulldown: true,
      pullup: true,
      dataList: [],
      page: 0,
      // 查询条件
      searchData: {
        personCardNum: '',
        personSex: '',
        personRace: '',
        personType: ''
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
      this.setSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    // 给查询条件信息赋值
    setSearchData () {
      let dictionary = JSON.parse(getStore('dictionary'))
      this.reqParam = JSON.parse(getStore('searchParams'))
      this.searchData.personCardNum = this.reqParam.personCardnum || '无'
      if (this.reqParam.personSexs) this.searchData.personSex = this.getArrVal(this.reqParam.personSexs[0], dictionary.sexList)
      if (this.reqParam.personRaces) this.searchData.personRace = this.getArrVal(this.reqParam.personRaces[0], dictionary.raceList)
      if (this.reqParam.personRegioncodes) this.searchData.personType = this.getArrVal(this.reqParam.personRegioncodes[0], dictionary.regionList)
      this.querySurveyList(this.reqParam)
    },
    // 查询布控信息列表
    querySurveyList (params) {
      this.$ajax(this.$api.getMoniForPage, params).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.dataList = this.dataList.concat(res.content.list)
            this.dataList.forEach(item => {
              item.personPhoto = this.imgJoint(item.personPhotoUrl)
            })
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.text('获取信息失败!', 'middle', 'warn')
        }
      )
    },
    // 循环数组取出对应的值
    getArrVal (val, arr) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].key === val) {
          return arr[i].value
        }
      }
    },
    // 加载更多方法
    loadMore (p) {
      this.reqParam.page = p
      this.loading = true
      this.querySurveyList(this.reqParam)
    },
    // 查看详细信息
    toChildPage (data) {
      setStore('searchData', data)
      this.$router.push({
        name: 'survey-person'
      })
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
