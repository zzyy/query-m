import { Scroll, Loading } from 'components'
import { getStore } from '../../../../../utils/storage'
export default {
  name: 'traffic-detail',
  data () {
    return {
      loading: true,
      crossData: [],
      dataList: [],
      pullup: true,
      page: 0,
      // 查询条件
      detailData: {},
      releaseParams: { startTime: '', endTime: '', personName: '', personCardnum: '', isEnable: '', personRaces: [], personRegioncodes: [], personSexs: [], personIds: [], moniId: [], page: 1, size: 10 },
      crossParams: { moniId: '', page: 1, size: 10 }
    }
  },
  mounted () {
    this.$nextTick(function () {
      this.$refs.wrapper._initScroll()
      this.getSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    getSearchData () {
      this.detailData = JSON.parse(getStore('detailData'))
      this.releaseParams.moniId.push(this.detailData.personId)
      this.crossParams.moniId = this.detailData.personId
      this.querySurveyList()
    },
    // 查询布控信息列表
    querySurveyList () {
      this.$ajax(this.$api.getMoniForPage, this.releaseParams).then(
        res => {
          if (res.code === 200) {
            if (res.content.count) {
              this.dataList = res.content.list[0]
            }

            this.queryPersonCross()
            // this.dataList.forEach(item => {
            //   item.personPhoto = this.imgJoint(item.personPhotoUrl)
            // })
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.text('获取信息失败!', 'middle', 'warn')
        }
      )
    },
    // 查询人员轨迹
    queryPersonCross () {
      this.$ajax(this.$api.getCrossByMoni, this.crossParams).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.crossData = this.crossData.concat(res.content.list)
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        }
      )
    },
    // 加载更多方法
    loadMore (p) {
      this.crossParams.page = p
      this.loading = true
      this.queryPersonCross()
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
