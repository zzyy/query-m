import { Scroll, Loading } from 'components'
import { setStore, getStore } from '../../../../utils/storage'
export default {
  name: 'traffic-survey',
  data () {
    return {
      loading: true,
      pullup: true,
      data: [1, 2, 3, 4, 5, 6],
      dataList: [],
      page: 0,
      // 查询条件
      searchData: {},
      comparisonParams: { startTime: '', endTime: '', verifyScore: '', verifyPersonId: '', passrecordId: '', verifyPersonName: '', personCardnum: '', verifyMode: '', passPlaceId: '', page: 1, size: 10 }
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
      this.getSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    getSearchData () {
      this.searchData = JSON.parse(getStore('searchData'))
      this.comparisonParams.passrecordId = this.searchData.id
      this.queryComparisonList()
    },
    // 查询人员布控信息
    queryComparisonList () {
      this.$ajax(this.$api.getListForRsult, this.comparisonParams).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.dataList = this.dataList.concat(res.content.list)
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        }
      )
    },
    // 加载更多方法
    loadMore (p) {
      this.comparisonParams.page = p
      this.loading = true
      this.queryComparisonList()
    },
    toChildPage (data) {
      setStore('detailData', data)
      this.$router.push({
        name: 'traffic-detail'
      })
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
