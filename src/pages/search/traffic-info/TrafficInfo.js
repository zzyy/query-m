import { Scroll, Loading } from 'components'
import { setStore, getStore } from '../../../utils/storage'
export default {
  name: 'traffic-info',
  data () {
    return {
      loading: true,
      pulldown: true,
      pullup: true,
      dataList: [],
      page: 0,
      // 查询条件
      searchData: {
        personCardNum: '',
        place: '',
        startTime: '',
        endTime: ''
      }
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
      this.setSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    // 给查询条件信息赋值
    setSearchData () {
      let places = JSON.parse(getStore('places'))
      this.reqParam = JSON.parse(getStore('searchParams'))
      this.searchData.personCardNum = this.reqParam.personCardnum || '无'
      this.searchData.startTime = this.reqParam.sTime
      this.searchData.endTime = this.reqParam.eTime
      if (this.reqParam.passPlaceId) this.searchData.place = this.getArrVal(this.reqParam.passPlaceId, places)

      this.queryPassList(this.reqParam)
    },
    // 查询通行信息列表
    queryPassList (params) {
      this.$ajax(this.$api.getCrossForPage, params).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.dataList = this.dataList.concat(res.content.list)
            this.dataList.forEach(item => {
              item.passPic = this.imgJoint(item.passPicUrl)
              if (item.passPersonCardnum === '') {
                item.passPersonCardnum = '无'
              }
            })
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.text('获取信息失败!', 'middle', 'warn')
        }
      )
    },
    // 循环数组取出对应的值
    getArrVal (val, arr) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].id === val) {
          return arr[i].placeNameCn
        }
      }
    },
    // 加载更多方法
    loadMore (p) {
      this.reqParam.page = p
      this.loading = true
      this.queryPassList(this.reqParam)
    },
    // 查看详细信息
    toChildPage (data) {
      setStore('searchData', data)
      this.$router.push({
        name: 'traffic-survey'
      })
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
