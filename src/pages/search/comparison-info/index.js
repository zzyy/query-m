import ComparisonInfo from './ComparisonInfo.vue'
import ComparisonSurvey from './comparison-survey'
export {
  ComparisonInfo,
  ComparisonSurvey
}
