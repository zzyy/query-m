import { getStore } from '../../../../utils/storage'
import { Loading } from 'components'
export default {
  name: 'comparison-survey',
  data () {
    return {
      loading: true,
      moniInfo: {},
      passcord: {},
      searchData: {
        passrecordId: '',
        foreignId: '',
        reginCode: '',
        veryfiMode: ''
      }
    }
  },
  components: {
    Loading
  },
  mounted () {
    this.$nextTick(function () {
      this.setSearchData()
    })
  },
  methods: {
    // 给查询条件信息赋值
    setSearchData () {
      this.reqParam = JSON.parse(getStore('searchData'))
      this.searchData.passrecordId = this.reqParam.passrecordId
      this.searchData.foreignId = this.reqParam.personId
      this.searchData.reginCode = this.reqParam.verifyPersonRegioncode
      this.searchData.veryfiMode = this.reqParam.verifyMode

      this.queryComparisonDetail(this.searchData)
    },
    // 查询比对信息详情列表
    queryComparisonDetail (params) {
      this.$ajax(this.$api.getResultForDetail, params).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.moniInfo = res.content.moniInfo
            this.passcord = res.content.passcord
            this.moniInfo.personPhoto = this.imgJoint(res.content.moniInfo.personPhotoUrl)
            this.passcord.passPic = this.imgJoint(res.content.passcord.passPicUrl)
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.text('获取信息失败!', 'middle', 'warn')
        }
      )
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
