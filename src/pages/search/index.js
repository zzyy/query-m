import Search from './Search.vue'
import { PersonInfo, PersonSurvey } from './person-info'
import { SurveyInfo, SurveyPerson } from './survey-info'
import { TrafficInfo, TrafficSurvey, TrafficDetail } from './traffic-info'
import { ComparisonInfo, ComparisonSurvey } from './comparison-info'
export {
  Search,
  PersonInfo,
  PersonSurvey,
  SurveyInfo,
  SurveyPerson,
  TrafficInfo,
  TrafficSurvey,
  TrafficDetail,
  ComparisonInfo,
  ComparisonSurvey
}
