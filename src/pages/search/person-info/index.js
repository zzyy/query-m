import PersonInfo from './PersonInfo.vue'
import PersonSurvey from './person-survey'
export {
  PersonInfo,
  PersonSurvey
}
