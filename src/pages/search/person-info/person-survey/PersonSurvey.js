import { Scroll, Loading } from 'components'
import { getStore } from '../../../../utils/storage'
export default {
  name: 'person-survey',
  data () {
    return {
      loading: true,
      pulldown: true,
      pullup: true,
      dataList: [],
      page: 0,
      // 查询条件
      searchData: {},
      releaseParams: { startTime: '', endTime: '', personName: '', personCardnum: '', isEnable: '', personRaces: [], personRegioncodes: [], personSexs: [], personIds: [], moniId: [], page: 1, size: 10 }
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
      this.getSearchData()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    getSearchData () {
      this.searchData = JSON.parse(getStore('searchData'))
      this.releaseParams.personIds.push(this.searchData.id)
      this.querySurveyList()
    },
    // 查询人员布控信息
    querySurveyList () {
      this.$ajax(this.$api.getMoniForPage, this.releaseParams).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.dataList = this.dataList.concat(res.content.list)
            this.dataList.forEach(item => {
              item.personPhoto = this.imgJoint(item.personPhotoUrl)
            })
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        }
      )
    },
    // 加载更多方法
    loadMore (p) {
      this.releaseParams.page = p
      this.loading = true
      this.querySurveyList()
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
