import { setStore, getStore } from '../../utils/storage'
import { dateFormat } from 'vux'
export default {
  name: 'search',
  data () {
    return {
      tabList: [
        {name: '人员信息', code: 'people'},
        {name: '布控信息', code: 'release'},
        {name: '通行信息', code: 'pass'},
        {name: '比对信息', code: 'comparison'}],
      clearText: '清空',
      minYear: 1949,
      verifyList: [{key: '0', value: '身份证比对'}, {key: '1', value: '图像比对'}],
      sexList: [],
      raceList: [],
      regionList: [],
      placeList: [],
      tabIndex: parseInt(getStore('tabIndex')),
      isCardSearch: true,
      time: {
        people: { startTime: '', endTime: '' },
        release: { startTime: '', endTime: '' },
        pass: { startTime: '', endTime: '' },
        comparison: { startTime: '', endTime: '' }
      },
      releaseTempParams: { personRace: '', personRegioncode: '', personSex: '' },
      peopleParams: { startTime: '', endTime: '', personName: '', personSex: '', personId: '', personRace: '', personRegioncode: '', personCardnum: '', page: 1, size: 10 },
      releaseParams: { startTime: '', endTime: '', personName: '', personCardnum: '', isEnable: '', personRaces: [], personRegioncodes: [], personSexs: [], personIds: [], moniId: [], page: 1, size: 10 },
      passParams: { startTime: '', endTime: '', passPersonId: '', passPlaceId: '', passrecordId: '', passPersonName: '', personCardnum: '', page: 1, size: 10 },
      comparisonParams: { startTime: '', endTime: '', verifyScore: '', verifyPersonId: '', passrecordId: '', verifyPersonName: '', personCardnum: '', verifyMode: '0', passPlaceId: '', page: 1, size: 10 },
      // 验证身份证号码
      cardNumValid: function (value) {
        let reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
        return {
          valid: reg.test(value),
          msg: '身份证输入不合法，请重新输入！'
        }
      },
      scoreValid: function (value) {
        // /^[1-9]+[0-9]*]*$/
        let reg = /^100$|^[0-9]+[0-9]*]*$/
        return {
          valid: reg.test(value),
          msg: '分数只能是小于等于100的整数'
        }
      }
    }
  },
  mounted () {
    this.getPublicDic()
    let index = getStore('tabIndex')
    if (index) {
      this.tabIndex = parseInt(index)
    }

    this.$nextTick(function () {
      let sevenDaysAgo = dateFormat(new Date() - 6 * 24 * 60 * 60 * 1000, 'YYYY-MM-DD')
      let currentTime = dateFormat(new Date(), 'YYYY-MM-DD')

      this.time.pass.startTime = sevenDaysAgo
      this.time.pass.endTime = currentTime
      this.time.comparison.startTime = sevenDaysAgo
      this.time.comparison.endTime = currentTime
    })
  },
  methods: {
    getPublicDic () {
      const all = {value: '全部', key: ''}
      const allPlace = {placeNameCn: '全部地点', id: ''}
      let publicDic = JSON.parse(getStore('dictionary'))
      this.sexList = publicDic.sexList
      this.raceList = publicDic.raceList
      this.regionList = publicDic.regionList

      // this.sexList.unshift(all)
      // this.raceList.unshift(all)
      this.regionList.unshift(all)

      // 查询地点
      this.$ajax(this.$api.getListForPlace).then(
        res => {
          if (res.code === 200) {
            this.placeList = res.content.entity
            this.placeList.unshift(allPlace)
            setStore('places', this.placeList)
          }
        }
      )
    },
    // 点击查询跳转到结果列表页面,这边仅将查询参数传到子页面，并不作查询
    toInfoList (page) {
      let storeParams = {}
      let peopleSTime = formatTime(this.time.people.startTime)
      let peopleETime = formatTime(this.time.people.endTime)
      let releaseSTime = formatTime(this.time.release.startTime)
      let releaseETime = formatTime(this.time.release.endTime)
      let passSTime = formatTime(this.time.pass.startTime)
      let passETime = formatTime(this.time.pass.endTime)
      let comparisonSTime = formatTime(this.time.comparison.startTime)
      let comparisonETime = formatTime(this.time.comparison.endTime)
      if (peopleSTime > peopleETime || releaseSTime > releaseETime || passSTime > passETime || comparisonSTime > comparisonETime) {
        this.$vux.toast.text('开始时间不能大于结束时间，请重新选择', 'middle')
        return false
      }

      this.peopleParams.startTime = peopleSTime
      this.peopleParams.endTime = peopleETime
      this.releaseParams.startTime = releaseSTime
      this.releaseParams.endTime = releaseETime
      this.passParams.startTime = passSTime === '' ? '' : passSTime + '000000'
      this.passParams.endTime = passETime === '' ? '' : passETime + '235959'
      this.comparisonParams.startTime = comparisonSTime === '' ? '' : comparisonSTime + '000000'
      this.comparisonParams.endTime = comparisonETime === '' ? '' : comparisonETime + '235959'
      // 人员信息查询
      switch (page) {
        case 'person-info':
          storeParams = this.peopleParams
          // 未格式化的时间
          storeParams.sTime = this.time.people.startTime
          storeParams.eTime = this.time.people.endTime
          break
        case 'survey-info':
          this.releaseParams.personRaces = []
          this.releaseParams.personSexs = []
          this.releaseParams.personRegioncodes = []
          if (this.releaseTempParams.personRace) this.releaseParams.personRaces.push(this.releaseTempParams.personRace)
          if (this.releaseTempParams.personSex) this.releaseParams.personSexs.push(this.releaseTempParams.personSex)
          if (this.releaseTempParams.personRegioncode) this.releaseParams.personRegioncodes.push(this.releaseTempParams.personRegioncode)
          storeParams = this.releaseParams
          // 未格式化的时间
          storeParams.sTime = this.time.release.startTime
          storeParams.eTime = this.time.release.endTime
          break
        case 'traffic-info':
          storeParams = this.passParams
          // 未格式化的时间
          storeParams.sTime = this.time.pass.startTime
          storeParams.eTime = this.time.pass.endTime
          break
        case 'comparison-info':
          if (this.comparisonParams.verifyScore > 100) {
            this.$vux.toast.text('比对分数不能大于100分', 'middle')
            return false
          }
          storeParams = this.comparisonParams
          // 未格式化的时间
          storeParams.sTime = this.time.comparison.startTime
          storeParams.eTime = this.time.comparison.endTime
          break
      }

      // 将查询参数存到本地
      setStore('searchParams', storeParams)
      // 跳转到对应页面
      this.$router.push({
        path: `/search/${page}`
      })
    },
    tabClickHandle (index) {
      this.tabIndex = index
      setStore('tabIndex', index)
    },
    // 切换比对类型改变分数是否禁用
    changeMode (val) {
      if (val === '1') {
        this.isCardSearch = false
      } else {
        this.isCardSearch = true
      }
    },
    // 清空时间控件
    clearTime (p) {
      if (p === 0) {
        switch (this.tabIndex) {
          case 0:
            this.time.people.startTime = ''
            break
          case 1:
            this.time.release.startTime = ''
            break
          case 2:
            this.time.pass.startTime = ''
            break
          case 3:
            this.time.comparison.startTime = ''
            break
        }
      } else {
        switch (this.tabIndex) {
          case 0:
            this.time.people.endTime = ''
            break
          case 1:
            this.time.release.endTime = ''
            break
          case 2:
            this.time.pass.endTime = ''
            break
          case 3:
            this.time.comparison.endTime = ''
            break
        }
      }
    }
  }
}
// 去掉时间格式中符号，以便传给后台
function formatTime (time) {
  if (time) {
    return time.replace(/-/g, '')
  } else {
    return ''
  }
}
