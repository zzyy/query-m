/**
 * Created by zzy on 2018/10/24.
 */
import Person from './person'
import PersonList from './person-list'
import HistoryDetail from './history-detail'
import { Search, PersonInfo, PersonSurvey, SurveyInfo, SurveyPerson, TrafficInfo, TrafficSurvey, TrafficDetail, ComparisonInfo, ComparisonSurvey } from './search'
import User from './user'

export {
  Person,
  PersonList,
  HistoryDetail,
  Search,
  PersonInfo,
  PersonSurvey,
  SurveyInfo,
  SurveyPerson,
  TrafficInfo,
  TrafficSurvey,
  TrafficDetail,
  ComparisonInfo,
  ComparisonSurvey,
  User
}
