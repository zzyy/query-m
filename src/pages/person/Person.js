import { TransferDom, dateFormat, AlertModule } from 'vux'
import { mapMutations } from 'vuex'
import { setStore } from '../../utils/storage'
import { Scroll, Loading } from 'components'
import lrz from 'lrz'
export default {
  name: 'person',
  directives: {
    TransferDom
  },
  data () {
    return {
      loading: false,
      dateTime: [
        ['1949', '1950', '1951', '1952', '1953', '1954', '1955', '1956', '1957', '1958',
          '1959', '1960', '1961', '1962', '1963', '1964', '1965', '1966', '1967', '1968',
          '1969', '1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978',
          '1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987', '1988',
          '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998',
          '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008',
          '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018',
          '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028'],
        ['1949', '1950', '1951', '1952', '1953', '1954', '1955', '1956', '1957', '1958',
          '1959', '1960', '1961', '1962', '1963', '1964', '1965', '1966', '1967', '1968',
          '1969', '1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977', '1978',
          '1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987', '1988',
          '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998',
          '1999', '2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008',
          '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018',
          '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028']],
      tabList: [
        {name: '查询', code: 'query'},
        {name: '历史', code: 'history'}],
      tabIndex: 0,
      timeTabIndex: 0,
      imageUrl: '',
      regionList: [],
      sexList: [],
      raceList: [],
      selectRegion: [],
      showPopup: false,
      regionText: '请选择比对库',
      regionValue: '',
      selectSex: null,
      selectRace: null,
      selectDate: [],
      timeRangeText: '选择时间范围',
      showTimePopup: false,
      historyList: [],
      hisReqData: {
        startTime: '',
        endTime: '',
        page: 1,
        size: 10
      },

      pulldown: true,
      pullup: true,
      // 第几页
      page: 0,
      nameValid: function (value) {
        let reg = /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{1,20}$/
        return {
          valid: reg.test(value),
          msg: '姓名格式不对哦～'
        }
      },
      // 验证身份证号码
      cardNumValid: function (value) {
        let reg = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
        return {
          valid: reg.test(value),
          msg: '身份证输入不合法，请重新输入！'
        }
      },
      // 人像查询参数
      reqData: {
        personPhoto: '',
        personRaces: [],
        personRegioncodes: [],
        personSexs: [],
        resultNum: '2',
        score: '70',
        search: {
          scoreConfirm: 70,
          createUserId: 0,
          searchBirthdayEnd: '',
          searchBirthdayStart: '',
          searchCardnum: '',
          searchName: '',
          searchRace: '',
          searchRegioncode: '',
          searchSex: ''
        },
        size: 10,
        page: 1
      }
    }
  },
  mounted () {
    this.$nextTick(function () {
      this.getPublicDataHandle()
    })
  },
  components: {
    Scroll,
    Loading
  },
  watch: {
    selectRegion (region) {
      let regionArr = []
      if (region.length) {
        for (let i = 0; i < region.length; i++) {
          for (let j = 0; j < this.regionList.length; j++) {
            if (region[i] === this.regionList[j].key) {
              regionArr.push(this.regionList[j].value)
            }
          }
        }
        this.regionText = regionArr.join(',')
      } else {
        this.regionText = '请选择比对库'
      }
    }
  },
  methods: {
    ...mapMutations([
      'PEOPLE_PHOTO_SEARCH'
    ]),
    // 获取公共接口
    getPublicDataHandle () {
      const all = {value: '全部', key: ''}
      this.$ajax(this.$api.getListForBase).then(
        res => {
          // res = JSON.parse(res)
          if (res.code === 200) {
            let content = res.content
            content.listDicInfo.forEach(list => {
              list.forEach(item => {
                let obj = {value: item.classNameCn, key: item.dicClassCode}
                if (item.dicCode === '1001') {
                  // races 民族
                  this.raceList.push(obj)
                } else if (item.dicCode === '1002') {
                  // sexs 性别
                  this.sexList.push(obj)
                }
              })
            })
            // 所属库
            let regionStore = []
            content.listRegion.forEach(item => {
              if (item.regionType !== 0 && item.regionLoadType !== 0) {
                this.regionList.push({value: item.regionName, key: item.regionRegioncode})
              }
              // 布控信息页面会用到
              if (item.regionType === 2 && item.regionLoadType !== 0) {
                regionStore.push({value: item.regionName, key: item.regionRegioncode})
              }
            })

            this.raceList.unshift(all)
            this.sexList.unshift(all)

            // 将字典对象存到store中
            let publicDiction = {
              raceList: this.raceList,
              sexList: this.sexList,
              regionList: regionStore
            }
            setStore('dictionary', publicDiction)
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.show({
            text: '获取业务字典及类型库失败！',
            type: 'warn',
            position: 'middle',
            width: 'auto'
          })
        }
      )
    },
    // 点击人像查询列表方法
    queryPerson () {
      let _self = this
      this.reqData.personRegioncodes = this.selectRegion
      if (this.selectSex) this.reqData.personSexs.push(this.selectSex)
      if (this.selectRace) this.reqData.personRaces.push(this.selectRace)

      getString(this.selectRegion, this.regionList).then(function (data) {
        _self.reqData.search.searchRegioncode = data
      })
      getString(this.reqData.personSexs, this.sexList).then(function (data) {
        _self.reqData.search.searchSex = data
      })
      getString(this.reqData.personRaces, this.raceList).then(function (data) {
        _self.reqData.search.searchRace = data
      })

      this.reqData.search.searchBirthdayStart = this.selectDate[0] || ''
      this.reqData.search.searchBirthdayEnd = this.selectDate[1] || ''

      if (this.reqData.personPhoto === '') {
        this.$vux.toast.text('请上传图片！', 'middle')
        return false
      }

      if (this.reqData.personRegioncodes.length === 0) {
        this.$vux.toast.text('请选择比对库！', 'middle')
        return false
      }

      let st = parseInt(this.selectDate[0])
      let et = parseInt(this.selectDate[1])
      if (st > et) {
        this.showAlert({title: '提示', content: '开始时间不能大于结束时间, 请重新选择', show: function () {}, hide: function () {}})
        return false
      }

      this.$router.push({
        name: 'person-list'
      })

      // setStore('peoplePhotoSearch', this.reqData)
      this.PEOPLE_PHOTO_SEARCH(this.reqData)
    },
    closePicker (flag) {
      if (flag) {
        // 确定
        let st = parseInt(this.selectDate[0])
        let et = parseInt(this.selectDate[1])
        if (st > et) {
          this.showAlert({title: '提示', content: '开始时间不能大于结束时间, 请重新选择', show: function () {}, hide: function () {}})
          return false
        }
      } else {
        // 取消
        this.selectDate = []
      }
    },
    // 历史记录列表加载方法
    onItemClick (index) {
      if (index === 1) {
        if (this.hisReqData.startTime === '') {
          this.historyQueryHandle(this.hisReqData)
        }
        this.$nextTick(() => {
          this.$refs.wrapper._initScroll()
        })
      }
    },
    openPopup () {
      this.showPopup = true
    },
    // 人像查询页面关闭人员类型选择器
    closePopup (bool) {
      this.showPopup = false
      if (!bool) {
        this.selectRegion = []
      }
    },
    // 历史记录页面打开时间选择器
    openTimePopup () {
      this.timeTabIndex = 0
      this.showTimePopup = true
      if (this.hisReqData.startTime === '') {
        this.hisReqData.startTime = dateFormat(new Date(), 'YYYY-MM-DD HH:mm')
      }
      if (this.hisReqData.endTime === '') {
        this.hisReqData.endTime = dateFormat(new Date(), 'YYYY-MM-DD HH:mm')
      }
    },
    // 历史记录页面关闭时间选择器
    closeTimePopup (bool) {
      if (bool) {
        let btf = this.hisReqData.startTime.replace(/-/g, '').replace(/ /g, '').replace(/:/g, '')
        let etf = this.hisReqData.endTime.replace(/-/g, '').replace(/ /g, '').replace(/:/g, '')
        if (btf > etf) {
          this.showAlert({title: '提示', content: '开始时间不能大于结束时间', show: function () {}, hide: function () {}})
          return false
        } else {
          this.timeRangeText = this.hisReqData.startTime + ' ~ ' + this.hisReqData.endTime
        }
        // 历史记录
        this.hisReqData.page = 1
        this.historyList = []
        this.historyQueryHandle(this.hisReqData)
      }
      // else {
      //   this.hisReqData.startTime = ''
      //   this.hisReqData.endTime = ''
      //   this.$refs.beginTime.render()
      //   this.$refs.endTime.render()
      //   this.timeRangeText = '选择时间范围'
      // }
      this.showTimePopup = false
    },
    // 查询历史记录回调函数
    historyQueryHandle (params) {
      this.loading = true
      this.$ajax(this.$api.getHistoryList, params).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.historyList = this.historyList.concat(res.content.historyList)
            this.historyList.forEach(item => {
              item.searchPhoto = this.imgJoint(item.searchPhotoUrl)
              item.resultPhoto = this.imgJoint(item.resultPhotoUrl)
            })
            if (res.content.historyList.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.show({
            text: '获取人像比对历史失败！',
            type: 'warn',
            position: 'middle',
            width: 'auto'
          })
        }
      )
    },
    // 查看详情
    toDetailPage (data) {
      setStore('historyData', data)
      this.$router.push({
        name: 'history-detail'
      })
    },
    // 加载更多方法
    loadMore (p) {
      this.hisReqData.page = p
      this.loading = true
      this.historyQueryHandle(this.hisReqData)
    },
    // 提示框
    showAlert (obj) {
      AlertModule.show({
        title: obj.title,
        content: obj.content,
        onShow () {
          obj.show()
        },
        onHide () {
          obj.hide()
        }
      })
    },
    // 上传图片
    uploadImgHandle (e) {
      let that = this
      let file = e.target.files[0]
      if (e.target.files.length === 0) return
      lrz(file)
        .then(function (rst) {
          // 处理成功会执行
          that.imageUrl = rst.base64
          // 赋值给查询条件中的人员图片
          setStore('searchImgBase', rst.base64.split('base64,')[0])
          that.reqData.personPhoto = rst.base64.split('base64,')[1]
        })
        .catch(function (err) {
          // 处理失败会执行
          console.log(err)
          this.$vux.toast.show({
            text: err,
            type: 'warn',
            position: 'middle',
            width: 'auto'
          })
        })
        .always(function () {
          // 不管是成功失败，都会执行
        })
    }
  }
}

// 将数组拼接成字符串
function getString (arrCode, arrObj) {
  return new Promise(function (resolve, reject) {
    let strArr = []
    arrCode.forEach(code => {
      arrObj.forEach(obj => {
        if (code === obj.key) {
          strArr.push(obj.value)
        }
      })
    })
    resolve(strArr.join('、'))
  })
}
