import { Scroll, Loading } from 'components'
import { mapState } from 'vuex'
import { getStore } from '../../utils/storage'
export default {
  name: 'person-list',
  data () {
    return {
      loading: true,
      showToast: false,
      pulldown: true,
      pullup: true,
      listData: [],
      page: 0,

      photoBase64: '',
      // 查询参数
      searchParams: {
        personPhoto: '',
        personRaces: [],
        personRegioncodes: [],
        personSexs: [],
        resultNum: '2',
        score: '70',
        search: {
          scoreConfirm: 70,
          createUserId: 0,
          searchBirthdayEnd: '',
          searchBirthdayStart: '',
          searchCardnum: '',
          searchName: '',
          searchRace: '',
          searchRegioncode: '',
          searchSex: ''
        },
        size: 10,
        page: 1
      },
      detailsData: {}
    }
  },
  mounted () {
    this.getPrams()
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
    })
  },
  components: {
    Scroll,
    Loading
  },
  computed: {
    ...mapState([
      'peoplePhotoSearch'
    ])
  },
  methods: {
    getPrams () {
      // 将带过来的条件赋值给变量
      this.searchParams = this.peoplePhotoSearch
      this.photoBase64 = getStore('searchImgBase') + 'base64,' + this.searchParams.personPhoto
      this.queryList(this.searchParams)
    },
    queryList (params) {
      this.$ajax(this.$api.getResultByHttp, params).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.listData = this.listData.concat(res.content.list)
            this.listData.forEach(item => {
              item.personPhoto = this.imgJoint(item.personPhotoUrl)
              // 将后台分数的类型转为int，防止环形图组件报错
              item.score = parseInt(item.score)
              if (item.score >= 90) {
                item.scoreColor = '#F76C18'
              } else {
                item.scoreColor = '#5884D6'
              }
            })
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          } else {
            this.$vux.toast.show({
              text: res.content,
              type: 'cancel',
              time: 3000,
              position: 'middle',
              width: 'auto'
            })
          }
        },
        err => {
          console.log('err', err)
          this.$vux.toast.show({
            text: '获取人员信息失败！',
            type: 'warn',
            position: 'middle',
            width: 'auto'
          })
        }
      )
    },
    queryDetail (data) {
      this.showToast = true
      // 蒙层显示，禁止scroll组件滚动
      this.$refs.wrapper.disable()
      document.body.style.position = 'fixed'
      this.detailsData = data
    },
    closeToast () {
      this.showToast = false
      // 蒙层显示，开启scroll组件滚动
      this.$refs.wrapper.enable()
      document.body.style.position = 'static'
    },
    // 滚动时改变头部透明度
    scrollHandle (pos) {
      // 滚动的距离
      let scrollDis = pos.y
      let bgOpacity = 0
      // let topHeight = this.$refs.top.clientHeight
      let headerHeight = this.$refs.header.clientHeight
      // 最大可滚动高度
      // let maxScrollHeight = topHeight - headerHeight

      if (scrollDis <= headerHeight) {
        bgOpacity = scrollDis / headerHeight
        if (bgOpacity > 0) {
          bgOpacity = 1
        } else {
          bgOpacity = Math.abs(bgOpacity)
        }
        this.headerBg = `rgba(95, 140, 214, ${bgOpacity})`
      }
    },
    // 加载更多方法
    loadMore (p) {
      this.searchParams.page = p
      this.loading = true
      this.queryList(this.searchParams)
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
