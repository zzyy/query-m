import { getStore } from '../../utils/storage'
export default {
  name: 'user',
  data () {
    return {
      userId: ''
    }
  },
  created () {
    this.userId = getStore('userId')
  }
}
