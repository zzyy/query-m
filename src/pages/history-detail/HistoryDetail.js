import { Scroll, Loading } from 'components'
// import { mapState } from 'vuex'
import { getStore } from '../../utils/storage'
export default {
  name: 'history-detail',
  data () {
    return {
      loading: true,
      showToast: false,
      pulldown: true,
      pullup: true,
      listData: [],
      page: 0,

      // 查询参数
      historySearch: {
        searchId: '',
        page: 1,
        size: 10
      },
      searchParams: {
        id: '',
        resultCnt: 0,
        resultPhoto: '',
        searchCardnum: '',
        searchName: '',
        searchPhoto: '',
        searchRace: '',
        searchRegioncode: '',
        searchSex: '',
        verifyMonitoringId: '',
        verifyScore: ''
      },
      detailsData: {}
    }
  },
  mounted () {
    this.getPrams()
    this.$nextTick(() => {
      this.$refs.wrapper._initScroll()
    })
  },
  components: {
    Scroll,
    Loading
  },
  methods: {
    getPrams () {
      this.searchParams = JSON.parse(getStore('historyData'))
      this.queryHistoryDetail(this.searchParams.id)
    },
    queryHistoryDetail (id) {
      this.historySearch.searchId = id
      this.$ajax(this.$api.getMoniListById, this.historySearch).then(
        res => {
          this.loading = false
          if (res.code === 200) {
            this.listData = this.listData.concat(res.content.list)
            this.listData.forEach(item => {
              item.searchPhoto = this.imgJoint(item.searchPhotoUrl)
              item.personPhoto = this.imgJoint(item.personPhotoUrl)
              // 将后台分数的类型转为int，防止环形图组件报错
              item.score = parseInt(item.score)
              if (item.score >= 90) {
                item.scoreColor = '#F76C18'
              } else {
                item.scoreColor = '#5884D6'
              }
            })
            // 显示更多
            if (res.content.list.length < res.content.size) {
              this.page = 0
            } else {
              this.page = res.content.pageNow || 0
            }
          }
        },
        err => {
          console.log(err)
          this.$vux.toast.show({
            text: '获取详情失败！',
            type: 'warn',
            position: 'middle',
            width: 'auto'
          })
        }
      )
    },
    // 加载更多方法
    loadMore (p) {
      this.historySearch.page = p
      this.loading = true
      this.queryHistoryDetail(this.searchParams.id)
    },
    queryDetail (data) {
      this.showToast = true
      // 蒙层显示，禁止scroll组件滚动
      this.$refs.wrapper.disable()
      document.body.style.position = 'fixed'
      this.detailsData = data
    },
    closeToast () {
      this.showToast = false
      // 蒙层显示，开启scroll组件滚动
      this.$refs.wrapper.enable()
      document.body.style.position = 'static'
    },
    // 滚动时改变头部透明度
    scrollHandle (pos) {
      // 滚动的距离
      let scrollDis = pos.y
      let bgOpacity = 0
      // let topHeight = this.$refs.top.clientHeight
      let headerHeight = this.$refs.header.clientHeight
      // 最大可滚动高度
      // let maxScrollHeight = topHeight - headerHeight

      if (scrollDis <= headerHeight) {
        bgOpacity = scrollDis / headerHeight
        if (bgOpacity > 0) {
          bgOpacity = 1
        } else {
          bgOpacity = Math.abs(bgOpacity)
        }
        this.headerBg = `rgba(95, 140, 214, ${bgOpacity})`
      }
    },
    // 路由返回上一层
    back () {
      this.$router.go(-1)
    }
  }
}
