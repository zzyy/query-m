vuex的代码通常放在src/store目录下
主要包含以下几个文件
index.js            入口文件
state.js            管理所有状态
mutations.js
mutation-types.js   存放一些mutation相关的字符串常量
actions.js          一些异步修改和对mutation的一些操作
getters.js          对state做一些映射


步骤：
1、在state.js中定义对象变量，导出。
2、在mutation-types中定义常量，比如要设置singer，就定义一个SET_SINGER的常量。
3、在mutations里面定义修改的操作。引入types中的常量，定义mutations对象，里面是修改方法。
4、映射修改的数据，在getters中做一层包装，通常是用getter去取我们的数据。
5、目前还没有异步修改的操作，所以actions暂时不用操作。现在直接在mutations中同步修改就可以。

在入口文件引入之后，在相关页面引入vuex提供的语法糖mapMutations，通过扩展运算符调用它，做对象映射setSinger: 'SET_SINGER',
然后在需要的地方调用this.setSinger(singer)

取值时，在computed中使用语法糖mapGetters(['singer'])，这里的singer对应到store/getter.js中的那个singer，
然后就可以this.singer使用它
