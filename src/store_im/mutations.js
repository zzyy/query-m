import * as types from './mutation-types'

const mutations = {
  // 这里第一个参数state是获取当前状态树上的state，singer是我们传的参数
  [types.SET_SINGER] (state, singer) {
    state.singer = singer
  }
}

export default mutations
