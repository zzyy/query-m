/**
 * Created by zzy on 2018/11/14.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex)

const state = {
  peoplePhotoSearch: {} // 人像查询条件
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
