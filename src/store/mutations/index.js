/**
 * Created by zzy on 2018/11/14.
 */
import {
  PEOPLE_PHOTO_SEARCH
} from '../mutation-types'
import { setStore } from '../../utils/storage'

export default {
  [PEOPLE_PHOTO_SEARCH] (state, peoplePhotoSearch) {
    state.peoplePhotoSearch = peoplePhotoSearch
    setStore('peoplePhotoSearch', state.peoplePhotoSearch)
  }
}
