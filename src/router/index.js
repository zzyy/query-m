import Vue from 'vue'
import Router from 'vue-router'
import {
  Person,
  PersonList,
  HistoryDetail,
  Search,
  PersonInfo,
  PersonSurvey,
  SurveyInfo,
  SurveyPerson,
  TrafficInfo,
  TrafficSurvey,
  TrafficDetail,
  ComparisonInfo,
  ComparisonSurvey,
  User
} from 'pages'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/person'
    },
    {
      path: '/person',
      name: 'person',
      component: Person,
      children: [
        {
          path: 'history-detail',
          name: 'history-detail',
          component: HistoryDetail
        }
      ]
    },
    {
      path: '/person-list',
      name: 'person-list',
      component: PersonList
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
      children: [
        {
          path: 'person-info',
          component: PersonInfo,
          children: [
            {
              path: 'person-survey',
              name: 'person-survey',
              component: PersonSurvey
            }
          ]
        },
        {
          path: 'survey-info',
          component: SurveyInfo,
          children: [
            {
              path: 'survey-person',
              name: 'survey-person',
              component: SurveyPerson
            }
          ]
        },
        {
          path: 'traffic-info',
          component: TrafficInfo,
          children: [
            {
              path: 'traffic-survey',
              name: 'traffic-survey',
              component: TrafficSurvey,
              children: [
                {
                  path: 'traffic-detail',
                  name: 'traffic-detail',
                  component: TrafficDetail
                }
              ]
            }
          ]
        },
        {
          path: 'comparison-info',
          component: ComparisonInfo,
          children: [
            {
              path: 'comparison-survey',
              name: 'comparison-survey',
              component: ComparisonSurvey
            }
          ]
        }
      ]
    },
    {
      path: '/user',
      name: 'user',
      component: User
    }
  ]
})

export default router
