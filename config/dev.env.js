'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API_URL: '"https://jxt.jsga.gov.cn:10006/app/jrface"',
  API_URL: '"http://192.168.1.16:8081"',
  TOKEN: '"query_eyJhbGciOiJIUzUxMiJ9.eyJyYW5kb21LZXkiOiI0NjJneGciLCJzdWIiOiIxMDAxIiwiZXhwIjoxNTQ4NjM5MjgwLCJpYXQiOjE1NDgwMzQ0ODB9.MRdZgoio8IvASIRerj8CXe7Q17n9n_JMtVTrML_N9YUbFrkWOYLJZJ4UB-4SjHz97yzepkKJvze-txMX38sMHw"'
})
